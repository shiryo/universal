<?php
$EM_CONF[$_EXTKEY] = array(
    'title' => 'Universal Extension',
    'description' => 'It\'s Universal',
    'category' => '',
    'author' => 'Markus Kugler',
    'author_mail' => 'dev@ma-ku.eu',
    'state' => 'experimental',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.1',
    'constraints' => array(
        'depends' => array(
            'typo3' => '7.6.0-7.6.99'
        ),
        'conflicts' => array(

        ),
        'suggests' => array(

        ),
    )
);
