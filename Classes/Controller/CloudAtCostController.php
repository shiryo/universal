<?php
namespace MK\Universal\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Messaging\FlashMessage;

/**
 * DefaultController
 */
class CloudAtCostController extends AbstractController
{
    /**
     * @var string
     */
    private $apiKey = '';
    
    /**
     * @var string
     */
    private $login = '';
    
    /**
     * Set often used values
     */
    public function initializeAction()
    {
        $extensionConfiguration = $this->getExtensionConfiguration();
        
        $this->apiKey = $extensionConfiguration['apiKeyCaC'];
        $this->login = $extensionConfiguration['loginCac'];
    }
    
    /**
     * Display main control page
     *
     * @return void
     */
    public function showAction()
    {
        $cacServerList = $this->getCaCServerList();
        
        $this->createDocHeader();
    
        $this->view->assign('serverList', $cacServerList);
    }
    
    /**
     * Power operations to start and stop the server
     *
     * @param string $serverId
     * @param string $operation
     * @return void
     */
    public function powerOperationAction($serverId = '', $operation = '') {
        $response = $this->powerOperation(
            $serverId,
            $operation
        );
        
        $this->handleAPIresponse($response);
        
        $this->redirect('show');
    }
    
    /**
     * Renames a server
     *
     * @param string $serverId
     * @param string $serverName
     */
    public function renameServerAction($serverId = '', $serverName = '') {
        if ($serverId == '' || $serverName == '') {
            $this->addFlashMessage('Some data was not given. Operation failed', 'Problem occurred', FlashMessage::ERROR);
            $this->redirect('show');
        }
        
        $response = $this->renameServer($serverId, $serverName);
        
        $this->handleAPIresponse($response);
        
        $this->redirect('show');
    }
    
    /**
     * Adds flashmessages according to the response data
     *
     * @param array $response
     *
     * @return void
     */
    private function handleAPIresponse($response = array()) {
        if ($response['result'] == "successful") {
            $this->addFlashMessage(
                LocalizationUtility::translate('tx_universal.serverOperation.successful', $this->extensionName),
                LocalizationUtility::translate('tx_universal.serverOperation.info', $this->extensionName),
                FlashMessage::OK
            );
        } else {
            $this->addFlashMessage(
                LocalizationUtility::translate('tx_universal.serverOperation.failure', $this->extensionName),
                LocalizationUtility::translate('tx_universal.serverOperation.info', $this->extensionName),
                FlashMessage::ERROR
            );
        }
    }
    
    /**
     * Rename Server
     *
     * @param string $serverId
     * @param string $newName
     *
     * @return array
     */
    private function renameServer($serverId = '', $newName = '') {
        $params = array(
            'key' => $this->apiKey,
            'login' => $this->login,
            'sid' => $serverId,
            'name' => $newName
        );
    
        $defaults = array(
            CURLOPT_URL => 'https://panel.cloudatcost.com/api/v1/renameserver.php',
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true
        );
        
        return $this->callAPI($params, $defaults);
    }
    
    /**
     * returns the server list from the cloud at cost api
     *
     * @return array
     */
    private function getCaCServerList() {
        return json_decode(
            file_get_contents(
                'https://panel.cloudatcost.com/api/v1/listservers.php?key=' . $this->apiKey . '&login=' . $this->login
            )
        );
    }
    
    /**
     * Operates Server power control
     *
     * @param string $serverId
     * @param string $operation
     * @return array
     */
    private function powerOperation($serverId = '', $operation = '') {
        $params = array(
            'key' => $this->apiKey,
            'login' => $this->login,
            'sid' => $serverId,
            'action' => $operation
        );
        
        $defaults = array(
            CURLOPT_URL => 'https://panel.cloudatcost.com/api/v1/powerop.php',
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true
        );
        
        return $this->callAPI($params, $defaults);
    }
    
    /**
     * Calls the API using curl
     *
     * @param array $postData
     * @param array $options
     *
     * @return array
     */
    private function callAPI($postData = array(), $options = array()) {
        $curl = curl_init();
        
        $options[CURLOPT_POSTFIELDS] = $postData;
        
        curl_setopt_array($curl, $options);
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return (array) json_decode($result);
    }
    
    /**
     * returns the extension configuration from the globals
     * possible keys are:
     * [loginCaC]
     * [apiKeyCac]
     *
     * @return array
     */
    private function getExtensionConfiguration() {
        return unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['universal']);
    }
}