<?php
namespace MK\Universal\ViewHelper;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * CalculatePercentageViewHelper
 */
class CalculatePercentageViewHelper extends AbstractViewHelper
{
    /**
     * Initialize all arguments. You need to override this method and call
     * $this->registerArgument(...) inside this method, to register all your arguments.
     *
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('baseValue', 'float', 'Represents 100%', true);
        $this->registerArgument('value', 'float', 'Value that should be recalculated to percentage', true);
    }
    
    /**
     * Returns argument value as percentage from baseValue
     *
     * @return float
     */
    public function render()
    {
        return $this->arguments['value'] * 100 / $this->arguments['baseValue'];
    }
}