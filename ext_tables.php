<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE === 'BE') {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'MK.' . $_EXTKEY,
        'tools',
        'moduleUniversal',
        '',
        array(
            'Default' => 'overview',
            'CloudAtCost' => 'show, powerOperation, renameServer',
        ),
        array(
            'access' => 'user,group',
            'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_moduleUniversal.xlf'
        )
    );
}
